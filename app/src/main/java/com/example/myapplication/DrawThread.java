package com.example.myapplication;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class DrawThread extends Thread {
    MySurface mySurface;
    SurfaceHolder surfaceHolder;
    boolean isRun = false;
    long nowTime, prevTime, elapsedTime;

    public DrawThread(MySurface mySurface,
                      SurfaceHolder surfaceHolder){
        this.mySurface = mySurface;
        this.surfaceHolder = surfaceHolder;
         prevTime = System.currentTimeMillis();
    }

    public void isRun(boolean run){
        isRun = run;
    }

    @Override
    public void run(){
        Canvas canvas;
        while (isRun){
            if (! surfaceHolder.getSurface().isValid()){
                continue;
            }
            canvas = null;
            nowTime = System.currentTimeMillis();
            elapsedTime = nowTime - prevTime;
            if (elapsedTime > 30){
                prevTime = nowTime;
                canvas = surfaceHolder.lockCanvas(null);
                synchronized (surfaceHolder){
                    mySurface.draw(canvas);
                }
                if (canvas != null){
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
